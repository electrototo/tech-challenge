import argparse
import pandas as pd
import numpy as np

import requests
from requests.compat import urljoin


def create_payload(row):
    data = {
        'code': row['d_codigo'],
        'settlement': row['d_asenta'],
        'settlement_type': row['d_tipo_asenta'],
        'municipality': row['D_mnpio'],
        'city': row['d_ciudad'],
        'zone': row['d_zona'],
        'state': row['c_estado'],
        'asenta_id': row['id_asenta_cpcons']
    }

    return data


ap = argparse.ArgumentParser()
ap.add_argument('-f', '--file', required=True, help='Path to the xlsx containg the CP codes')
ap.add_argument('-s', '--host', required=True, help='Host in which the data will be sent')
ap.add_argument('-p', '--sheets', required=False, help='Sheets to export, default: all')

args = vars(ap.parse_args())

sheet_names = None
if args['sheets'] is not None:
    sheet_names = args['sheets'].split(',')

df_dict = pd.read_excel(args['file'], sheet_name=sheet_names, dtype={'d_codigo': str})


for state, df in df_dict.items():

    df['d_ciudad'] = df['d_ciudad'].replace({np.nan: None})

    # Assuming the database is on a blank state, the states will not be
    # created
    state_created = True
    for index, row in df.iterrows():

        data = create_payload(row)

        # Create the state if it does not exists
        if not state_created:
            state_data = {
                'id': data['state'],
                'name': state.replace('_', ' ')
            }

            sr = requests.post(
                urljoin(args['host'], 'states/'),
                json=state_data
            )

            if sr.status_code == 201:
                state_created = True

        try:
            # Create the zip code entity
            zr = requests.post(
                urljoin(args['host'], 'zip-codes/'),
                json=data
            )

            if zr.status_code != 201:
                print(urljoin(args['host'], 'zip-codes/'))
                print('Error', data)
                print(zr.content)
        except Exception as e:
            print(urljoin(args['host'], 'states'))
            print(e)
            print(data)
