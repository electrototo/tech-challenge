# Menos es más, pensando fuera de la caja

from fastapi import FastAPI

from pydantic import BaseModel
from typing import List, Optional

import pandas as pd
import numpy as np


xls_db = './CPdescarga.xls'

df_dict = pd.read_excel(xls_db, sheet_name=None, dtype={'d_codigo': str})

# Concatenamos todas las hojas
db = pd.concat(df_dict.values())
db['d_ciudad'] = db['d_ciudad'].replace({np.nan: None})

app = FastAPI()


class State(BaseModel):
    id: int
    name: str


class PostalService(BaseModel):
    code: str
    settlement: str
    settlement_type: str
    municipality: str
    city: Optional[str] = None
    zone: str
    state: State


class PostalAnswer(BaseModel):
    status: bool = True
    payload: List[PostalService]


def create_payload(row):
    data = {
        'code': row['d_codigo'],
        'settlement': row['d_asenta'],
        'settlement_type': row['d_tipo_asenta'],
        'municipality': row['D_mnpio'],
        'city': row['d_ciudad'],
        'zone': row['d_zona'],
        'state': {
            'id': row['c_estado'],
            'name': row['d_estado']
        }
    }

    return data


@app.get('/zip-codes/{zip_code}', response_model=PostalAnswer)
async def list_zip_codes(zip_code: str):

    filt = db[db['d_codigo'] == zip_code]
    items = []

    for i, row in filt.iterrows():
        items.append(create_payload(row))

    return {'payload': items}
