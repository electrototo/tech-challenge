# Información del repositorio

En este repositiorio se pueden encontrar 2 submodulos: `cp_dep_1` y `cp_dep_3`, 1 carpeta con una aplicaciónd de fastapi: `cp_dep_2`, la base de datos (`CPdescarga.xls`) que fue utilizada para llenar las tablas de dynamodb (más adelante) y la base de datos de rds, y un script auxiliar (`database_loader.py`) para utilizar los endpoints y cargar los datos en las respectivas bases de datos, utilizando las respectivas apis.

## cp_dep_1

Este submodulo resuelve el problema utilizando el siguiente stack: FastAPI (magum), AWS DynamoDB y AWS Lambda.

## cp_dep_2

Esta carpeta incluye el código (no en producción) de una API que resuelve el problema utilizando FastAPI y pandas. Sencillamente carga el archivo en memoria, filtra los registros y regresa los resultados encontrados.

## cp_dep_3

Este submodulo resuleve el problema utilizando el siguiente stack: Django (zappa), AWS RDS, AWS S3, y AWS Lambda.


Dentro de los submodulos se encuentra una explicación más detallada.

## database_loader.py

Cree este programa para poder cargar la información a los endpoints correspondientes. Se utiliza de la siguiente manera:

```bash
$ python database_loader.py -f CPdescarga.xls -p Distrito_Federal -s {ENDPOINT}
```

Parámetros
* `-f`:   Especifica el archivo a leer.
* `-p`:   Especifica qué hojas se desean cargar, separadas por una coma. El parámetro es opcional, si se omite, se usan todas las hojas.
* `-s`:   Especifica el endpoint a utilizar.
